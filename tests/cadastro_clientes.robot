***Settings***
Documentation       Cadastro de clientes

Resource    ../resources/base.robot

Test Setup          Login Session
Test Teardown       Finish Session

***Test Cases***
Novo cliente
    Dado que acesso o formulário de cadastro de clientes
    Quando faço a inclusão desse cliente:
    ...         Bon Jovi        00000001406     Rua dos Bugs, 1000      11999999999
    Então devo ver a notificação:   Cliente cadastrado com sucesso!

Campos Obrigatórios
    [tags]      temp
    Dado que acesso o formulário de cadastro de clientes
    Quando faço a inclusão desse cliente:
    ...         ${EMPTY}        ${EMPTY}     ${EMPTY}      ${EMPTY}
    Então ver mensagens informando que os campos do cadastro de cliente são obrigatórios

# Nome é obrigatório
# Cpf é obrigatório
# Endereço é obrigatório
# Telefone é obrigatório
# 1o Desafio do RoboCamp 6

# Implementar os cenários de campos obrigatórios usando a técnica do Test Template
# Premio: Os 5 primeiros que entregarem até o dia 27 de Agosto de 2020 as 19h, ganharão uma vaga
# No curso Performance Tests (o estado da arte das aplicações)

# Entrega:
# Subir no gitlab, e enviar o link nos comentários da Aula da Live de Hoje!